import React from 'react';
import PropTypes from 'prop-types';
import {
  CollapsedEditor,
  Editor,
  EditorContext,
  WithEditorActions,
} from '@atlaskit/editor-core';
import { WikiMarkupTransformer } from '@atlaskit/editor-wikimarkup-transformer';

function Form () {
  const [state,setState] = React.useState({isExpanded: false});

  const toggleEditor = () => {
    setState({
      isExpanded: !state.isExpanded,
    });
  };

  const onCancel = () => {
    toggleEditor();
  };

  const onSave = async () => {
    console.log("saving in progress")
    /*
    const { actions, onSave } = this.props;
    const value = await actions.getValue();

    if (value) {
      actions.clear();
      onSave(value);
    }
    */
  };

    const { isExpanded } = state;
    const isSending =false;

    return (
      <div style={{ marginTop: 20 }}>
        <CollapsedEditor
          placeholder="Write a comment"
          isExpanded={isExpanded}
          onFocus={toggleEditor}
        >
          <Editor
            allowCodeBlocks
            shouldFocus
            disabled={isSending}
            appearance="comment"
            contentTransformerProvider={schema =>
              new WikiMarkupTransformer(schema)
            }
            onSave={onSave}
            onCancel={onCancel}
          />
        </CollapsedEditor>
      </div>
    );
  
}

/* Form.propTypes = {
  actions: PropTypes.shape({
    clear: PropTypes.func,
    getValue: PropTypes.func,
  }).isRequired,
  isSending: PropTypes.bool,
  onSave: PropTypes.func.isRequired,
}; */

/* Form.defaultProps = {
  isSending: false,
};*/


export default props => (
  <EditorContext>
    <WithEditorActions
      render={actions => <Form {...props} actions={actions} />}
    />
  </EditorContext>
);
