import * as React from 'react';
import Form from './Form';

export default function NewPost() {


  return (
    <div id="discussion-form">
      <Form />
    </div>
  );
}

