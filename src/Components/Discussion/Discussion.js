import * as React from 'react';
import PropTypes from 'prop-types';
import NewPost from './NewPost';

import PostsList from './PostsList';

export default function Discussion(props) {
  const requestId=123 //devra etre remplacer par props.requestId
  
  //fetch all existing comment and send them to PostsList

  //catch the event when a new post is done (call API to write message in DB).
  // Once it is written, refresh the posts list and send it again to PostsList

  return (
    <div id="discussion">
      <PostsList id={requestId} />
      <NewPost id={requestId} />
    </div>
  );
}

/*      <NewPost id={requestId} />*/