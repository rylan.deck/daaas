import * as React from 'react';
import PropTypes from 'prop-types';
import EmptyChatIcon from '@atlaskit/icon/glyph/media-services/add-comment';
import { Code } from 'react-content-loader';
import { akColorN70 } from '@atlaskit/util-shared-styles';

import Post from './post';
import * as styles from './styles.css';

export default function PostsList() {

  const data=[{'_id':'1','comment':'bien le bonjour','authorUser': 'myemailuser@canada.ca','createdTs':'2020/08/10 11:11:00.123','isSaving':false},
  {'_id':'2','comment':'Salut!','authorUser': 'otheruser@canada.ca','createdTs':'2020/08/11 11:11:00.123','isSaving':false},
  {'_id':'3','comment':'Testing.Test','authorUser': 'TestEmail@canada.ca','createdTs':'2020/08/11 11:11:00.123','isSaving':false}]

  if (data.length <= 0) {
    return (
      <div className={styles.empty}>
        <EmptyChatIcon size="xlarge" primaryColor={akColorN70} />
        <h5>No posts yet</h5>
        <p>Be the first to add to the discussion regarding this review!</p>
      </div>
    );
  }

  /* a remplacer

  if (fetchStatus === 'loading') {
    return (
      <div className={styles.loading}>
        {[1, 2, 3].map(n => (
          <div key={n} className={styles.loadingPost}>
            <Code />
          </div>
        ))}
      </div>
    );
  } */

  return (
    <div id="discussion-posts-list" className={styles.list}>
      {data.map(d => <Post key={d._id} data={d} />)}
    </div>
  );
}

/* PostsList.propTypes = {
  data: PropTypes.arrayOf(
    PropTypes.shape({
      _id: PropTypes.string.isRequired,
      authorUser: PropTypes.string,
      createdTs: PropTypes.string,
      comment: PropTypes.string,
    })
  ).isRequired,
  fetchStatus: PropTypes.string.isRequired,
}; */

